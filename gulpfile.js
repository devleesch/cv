var gulp = require('gulp');
var inject = require('gulp-inject');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var rev = require('gulp-rev');
var del = require('del');

var dist = './dist';
var src = {
    js: ['./src/js/**/*.js'],
    css: ['./src/css/**/*.css'],
    assets: ['./src/assets/**/*'],
    vendors: {
        js: [
            './node_modules/particles.js/particles.js',
            './node_modules/materialize-css/dist/js/materialize.min.js'
        ],
        css: [
            './node_modules/materialize-css/dist/css/materialize.min.css'
        ]
    }
};

gulp.task('clean', function() {
    return del(dist + '**/*');
});

gulp.task('copy:assets', ['clean'], function() {
    return gulp.src(src.assets)
        .pipe(gulp.dest(dist + '/assets'));
});

gulp.task('build', ['copy:assets'], function() {
    return gulp.src('./src/index.html')

        .pipe(inject(
            gulp.src(src.vendors.js)
                .pipe(concat('vendors.js'))
                .pipe(rev())
                .pipe(gulp.dest(dist))
            , {name: 'vendors', ignorePath: dist.substring(1)}
        ))

        .pipe(inject(
            gulp.src(src.vendors.css)
                .pipe(concat('vendors.css'))
                .pipe(rev())
                .pipe(gulp.dest(dist))
            , {name: 'vendors', ignorePath: dist.substring(1)}
        ))

        .pipe(inject(
            gulp.src(src.js)
                .pipe(concat('app.js'))
                .pipe(rev())
                .pipe(gulp.dest(dist))
            , {ignorePath: dist.substring(1)}
        ))

        .pipe(inject(
            gulp.src(src.css)
                .pipe(concat('app.css'))
                .pipe(rev())
                .pipe(gulp.dest(dist))
            , {ignorePath: dist.substring(1)}
        ))

        .pipe(gulp.dest(dist));
});

gulp.task('dev', ['build'], function() {
    gulp.watch(['./src/index.html', src.assets, src.css, src.js], ['build']);
});

gulp.task('default', ['dev']);